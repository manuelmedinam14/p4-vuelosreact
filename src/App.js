import React,{Component} from 'react';
import './App.css';
import Header from './Componentes/Header';
import InfoVuelo from './Componentes/InfoVuelo';
import Formulario from './Componentes/Formulario';
import { ArreglarFecha } from './helper';

class App extends Component {

  state = {
    startDate: new Date(),
    consulta:{},
    llegada:{},
    salida:{},
    OperatingCarrier:'',
    Status:{},
    error:false,
    cargando:false
    
  };

  handleChange = (date) => { 
    this.setState({
      startDate: date
    });
  };

  consulta=consulta=>{
    if(consulta.nVuelo===''|| consulta.fVuelo===''){
      this.setState({
        error:true,
      })
    }else{
      this.setState({
        consulta,
        cargando:true
      })}
  }

  //si cambia
  componentDidUpdate( prevProps, prevState) {
    if(prevState.consulta !== this.state.consulta){
      this.consultarApi();
    }
  }

  consultarApi=()=>{
    let {nVuelo,fVuelo} =this.state.consulta;
    if(nVuelo==''||fVuelo=='') return null;
    let new_fVuelo = ArreglarFecha(fVuelo);

    //haciendo los fetch correspondientes
    const fetchAsync = async (nVuelo,new_fVuelo,consulta) =>{
      //consulto primero al fetch por toda la iformacion
      const response = await fetch(`https://api.lufthansa.com/v1/operations/customerflightinformation/${nVuelo}/${new_fVuelo}`, {
        headers: {
          'Authorization': 'Bearer jv9u2bb849pqgg9692m3psnd',
          'Accept': 'application/json'
        }
     })
     const data = await response.json()
     //verifico que no hay error
     if(data.hasOwnProperty('ProcessingErrors')){
      
      this.setState({
        error:true,
        cargando:false
      })
      return null;
      }
      else{
        //si no hay error saco la informacion del vuelo
        const FlightInformation = data['FlightInformation']['Flights']['Flight'];
        const{Arrival,Departure,OperatingCarrier,Status} = FlightInformation;

        //pregunto por el operador
        const aeroLineaResponse = await fetch(`https://api.lufthansa.com/v1/mds-references/airlines/${OperatingCarrier['AirlineID']}?limit=20&offset=0`, {
          headers: {
            'Authorization': 'Bearer jv9u2bb849pqgg9692m3psnd',
            'Accept': 'application/json'
              }
            })
            // parseo el operador
        const aeroLineaData = await aeroLineaResponse.json();
        //saco la aerolinea
        var Operador=aeroLineaData['AirlineResource']['Airlines']['Airline']['Names']['Name']['$']

        //saco los datos de salida
        const salidaResponsive =  await fetch(`https://api.lufthansa.com/v1/mds-references/airports/${Departure['AirportCode']}?lang=ES&limit=20&offset=0&LHoperated=0`, {
                    headers: {
                      'Authorization': 'Bearer jv9u2bb849pqgg9692m3psnd',
                      'Accept': 'application/json'
                    }
                })
        //parseo los datos de salida
        const salidaData = await salidaResponsive.json();
        Departure.AeropuertSalida=salidaData['AirportResource']['Airports']['Airport']['Names']['Name']['$']
        
        
        //sacos los datos de llegada
        const llegadaResponsive = await fetch(`https://api.lufthansa.com/v1/mds-references/airports/${Arrival['AirportCode']}?lang=ES&limit=20&offset=0&LHoperated=0`, {
            headers: {
              'Authorization': 'Bearer jv9u2bb849pqgg9692m3psnd',
              'Accept': 'application/json'
            }
          })
        //parseo los datos
        const llegadaData = await llegadaResponsive.json()
        Arrival.aeropuertoLlegada = await llegadaData['AirportResource']['Airports']['Airport']['Names']['Name']['$']

        await this.setState({
          Status,
          OperatingCarrier:Operador,
          salida:Departure,
          llegada:Arrival,
          error:false,
          cargando: false,
          consulta:consulta
        })
        }
      }

      try{
        fetchAsync(nVuelo,new_fVuelo,this.state.consulta)
      }
      catch(e){
        console.log(e)
      }
    }

    render() {
  
      const error = this.state.error;
      const cargando = this.state.cargando
      var resultado;
      if(cargando){
        resultado= <div className="spinner">
        <div className="double-bounce1"></div>
        <div className="double-bounce2"></div>
      </div>
      }else{
        if(error){
        resultado=<p className="white">Error</p>;
        }
        else{
          resultado= <InfoVuelo
          consulta={this.state.consulta}
          estado={this.state.Status}
          llegada={this.state.llegada}
          salida={this.state.salida}
          operador={this.state.OperatingCarrier}

         />;
        }
      }

        return (
          <div className="container-fluid">
          <div className=" row">
            <div className="col-lg-12 px-0">
              <Header
                
                titulo="Tracking Vuelos"
              />
            </div>
          </div> 
          
          <div className="row justify-content-center">
              <div className="p-3 col-lg-12 color-panel ">
                  <Formulario
                    fechaInicio={this.state.startDate}
                    cambioFecha={this.handleChange}
                    consulta={this.consulta}
                  />
              </div>
          </div>
          <div className="row">
            <div className="col-lg-12 ">
              {resultado}                
            </div>
          </div>
              
          
        </div>
        );
    }
}


export default App;
