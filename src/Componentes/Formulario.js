import React,{Component} from 'react';
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";
class Formulario extends Component {

    //coger Informacion
      numeroVuelo = React.createRef()
      fechaVuelo = React.createRef();     

      informacion=(e)=>{
        e.preventDefault();

        const consulta ={
            nVuelo:this.numeroVuelo.current.value,
            fVuelo:this.fechaVuelo.current.input.value
        }

        this.props.consulta(consulta)

      }
    render() {
        return (
            <form className="border-card" onSubmit={this.informacion}>
                <div className="row mr-0 ">
                    <div className="col-lg-5 col-md-12">
                        <div className="row justify-content-sm-center">
                            <div className="offset-1 col-lg-5 col-md-5">
                                <label className="tamaño-subtitulo white" htmlFor="Vuelo"><samp>Numero de vuelo</samp></label>
                                <input type="text" ref={this.numeroVuelo} placeholder="LH400" className="letra-mayuscula  form-control" id="Vuelo"/>
                                <small id="emailHelp" className="form-text text-muted">2 letras seguido de 4 o 3 numeros</small>
                            </div>
                            <div className="col-lg-4 col-md-5 col-sm-4 offset-1 px-0 align-self-center">
                                <h2 className="mt-3"><span className="badge badge-pill badge-warning">"LH400"</span></h2>
                            </div>
                        </div>
                        <div className="row mt-3 justify-content-sm-center">
                            <div className="offset-1 col-lg-6 col-md-5">
                                <label className="tamaño-subtitulo white"><samp>Fecha de vuelo</samp></label>
                                <DatePicker ref={this.fechaVuelo} className="form-control"
                                        selected={this.props.fechaInicio}
                                        onChange={this.props.cambioFecha}
                                    />
                                <small id="emailHelp" className="form-text text-muted">Puedes escribir en el campo </small>
                            </div>  
                            <div className="col-lg-4 col-md-5 col-sm-6 px-0 align-self-center">
                                <h2 className="mt-3"><span className="badge badge-pill badge-warning">"MM-DD-AAAA"</span></h2>
                            </div> 
                        </div>
                    </div>
                    <div className="col-lg-6 col-md-12 d-flex align-items-stretch">
                        <div className="col-lg-12 mb-2 border-left p-5 d-flex align-items-stretch">
                            <button type="submit" className="btn btn-warning btn-block tamaño-titulo">Consultar</button>
                        </div>  
                    </div>
                </div>
            </form>


        );
    }
}
export default Formulario