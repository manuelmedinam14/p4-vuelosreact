import React from 'react';

function Status(props) {
    var resultado='';
    switch (props.estado.Code) {
        case 'CD':
            resultado='Vuelo Cancelado'
            break;
        case 'DP':
            resultado='Vuelo Despegado'
            break;
        case 'LD':
            resultado='Vuelo Aterrizo'
            break;
        case 'RT':
            resultado='Vuelo Reencaminado'
            break;
        case 'DV':
            resultado='Vuelo Desviado'
            break;
        case 'HD':
            resultado='Gran Retraso'
            break;
        case 'FE':
            resultado='Vuelo Temprano'
            break;
        case 'DL':
            resultado='Vuelo Retrasado'
            break;
    
        default:
            resultado='No hay estado'
            break;
    }
    return (
        <React.Fragment>
            <div className="row justify-content-center">
                <div className="col-12 text-center">
                    <samp className="tamaño-titulo white">Estado de vuelo</samp>
                </div>
            </div>
            <dl className="row justify-content-center">
                <dt className="col-6 text-center">
                    <samp className="white">
                        {resultado}
                    </samp>
                </dt>  
            </dl>
        </React.Fragment>
    );
}

export default Status;