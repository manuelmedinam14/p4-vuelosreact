import React from 'react';
import '../App.css';

function Header(props) {
    return (
        <header className="col-lg-12 px-0">
            <nav className="navbar navbar-dark bg-dark justify-content-center">
                <a className="tamaño-titulo navbar-brand font-italic font-weight-bold">
                <img src="/docs/4.4/assets/brand/bootstrap-solid.svg" width="30" height="30" className="d-inline-block align-top" alt=""/>{props.titulo}</a>
            </nav>
        </header>
        
    );
}

export default Header;