import React,{Component} from 'react';
import InfoLlegada from './InfoLlegada';
import InfoSalida from './InfoSalida';
import Status from './Status';

class InfoVuelo extends Component {

    mostrarResultado=()=>{


        if(Object.keys(this.props.consulta).length === 0) return null;

        const {nVuelo,fVuelo} = this.props.consulta;        
        return (

        <div>   
        <div className="mt-3 row justify-content-center color-panel">
            <div className="col-lg-6 col-md-8">
                <nav className="navbar py-0 navbar-dark bg-warning border-card justify-content-center">
                    <a className="tamaño-titulo navbar-brand letra-mayuscula ">
                        <samp className="black">{nVuelo} ········· {fVuelo}</samp>
                    </a>
                </nav>
            </div>
        </div>

        <div className="row color-panel">
            <div className="col-lg-12 border-bottom border-warning">
                <div className="row mt-3 justify-content-center">
                         <dt className="offset-1 col-lg-2">
                            <samp className="white">
                                Aerolinea Operadora: 
                            </samp>
                        </dt>    
                        <dd className="col-lg-2">
                            <samp className="white">
                                {this.props.operador}
                            </samp>
                        </dd>                        
                </div>
            </div>
            <div className="offset-1 col-lg-5 border-right border-warning">
                <InfoSalida
                Salida={this.props.salida}
                />
            </div>
            <div className="col-lg-6">
                <InfoLlegada
                    llegada ={this.props.llegada}
                />
            </div>
            <div className="col-lg-12  border-top border-warning">
                <Status
                    estado = {this.props.estado}
                />
            </div>
                
            
        </div>
        </div>
    )
    }


    render() {
        var resultado =this.mostrarResultado();
        return (
            <div>
                {resultado}
            </div>
        );
    }
}

export default InfoVuelo