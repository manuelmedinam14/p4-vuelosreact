import React from 'react';

function InfoSalida(props) {
    var{Scheduled,AeropuertSalida,Terminal}=props.Salida
    let terminal='-'
    let puerta='-'
    if(Terminal!=undefined){
        terminal=Terminal['Name']
        puerta=Terminal['Gate']
    }
    return (
        <div className="row justify-content-center">
        <div className="col-lg-12 text-center">
            <samp className="tamaño-titulo white">Salida</samp>
        </div>
        <dl className="row justify-content-around">
             <dt className="col-5 offset-1">
                <samp className="white ">
                    Aeropueto de salida:  
                </samp>
            </dt>    
            <dd className="col-4">
                <samp className="white">
                    {AeropuertSalida}
                </samp>
            </dd>                        
             <dt className="col-5 offset-1">
                <samp className="white ">
                    hora de salida programada:  
                </samp>
            </dt>    
            <dd className="col-4">
                <samp className="white">
                    {Scheduled['Time']}
                </samp>
            </dd>                        
             <dt className="col-5 offset-1">
                <samp className="white ">
                    Fecha de salida programada:  
                </samp>
            </dt>    
            <dd className="col-4">
                <samp className="white">
                    {Scheduled['Date']}
                </samp>
            </dd>                        
             <dt className="col-5 offset-1">
                <samp className="white ">
                    Trminal:  
                </samp>
            </dt>    
            <dd className="col-4">
                <samp className="white">
                    {terminal}
                </samp>
            </dd>                        
             <dt className="col-5 offset-1">
                <samp className="white ">
                    Puerta:  
                </samp>
            </dt>    
            <dd className="col-4">
                <samp className="white">
                    {puerta}
                </samp>
            </dd>                        
        </dl>
    </div>
    );
}

export default InfoSalida;